jQuery(document).ready(function(){

$('#mini_chart select.posttype_dropdown').select2();

if ($("#chart-cont" ).length == 0 ) {
	return;
}

function generateChartData(i) {
    var chartData = [];
    
    if(jQuery('#myChart'+i).length > 0)
    { 
		var data_val = jQuery('#myChart'+i).data('stuff');
		var data_len =  data_val.length;
		if(data_len > 0)
		{
			for(var j = 0; j < data_len; j++)
			{
				chartData.push({
					date: data_val[j].date,
					visits: data_val[j].visits
				});
				
			}
		}		
	}
	else
	{
		return;
	}
    return chartData;
}

	var total_posts  = jQuery('#total_posts').val();
	var chart_type = jQuery('#chart_type').val();
	var stat_type = jQuery('#stat_type').val();
	var text_stat = '';
	if(stat_type == 'today')
	{
		text_stat = 'Hrs';
	}
	else if(stat_type == 'week' || stat_type == 'month')
	{
		text_stat = 'Date';	
	}
	else if(stat_type == 'year')
	{
		text_stat = 'Month';	
	}
	var borderwidth = 0;
	var bool = false;
	var flag = false;
	if(chart_type == 'bar')
	{
		borderwidth = 1;
		bool = false;
		flag = true;
	}
	else
	{
		borderwidth = 2,
		bool = false;
		flag = false;
	}
	
	Chart.defaults.global.pointHitDetectionRadius = 1;

	var customTooltips = function(tooltip) {
		// Tooltip Element
		var tooltipEl = document.getElementById('chartjs-tooltip');

		if (!tooltipEl) {
			tooltipEl = document.createElement('div');
			tooltipEl.id = 'chartjs-tooltip';
			tooltipEl.innerHTML = '<table></table>';
			this._chart.canvas.parentNode.appendChild(tooltipEl);
		}

		// Hide if no tooltip
		if (tooltip.opacity === 0) {
			tooltipEl.style.opacity = 0;
			return;
		}

		// Set caret Position
		tooltipEl.classList.remove('above', 'below', 'no-transform');
		if (tooltip.yAlign) {
			tooltipEl.classList.add(tooltip.yAlign);
		} else {
			tooltipEl.classList.add('no-transform');
		}

		function getBody(bodyItem) {
			return bodyItem.lines;
		}

		// Set Text
		if (tooltip.body) {
			var titleLines = tooltip.title || [];
			var bodyLines = tooltip.body.map(getBody);

			var innerHtml = '<thead>';

			titleLines.forEach(function(title) {
				innerHtml += '<tr><th>' + title + '</th></tr>';
			});
			innerHtml += '</thead><tbody>';

			bodyLines.forEach(function(body, i) {
				var colors = tooltip.labelColors[i];
				var style = 'background:' + colors.backgroundColor;
				style += '; border-color:' + colors.borderColor;
				style += '; border-width: 2px';
				var span = ' <span class="chartjs-tooltip-key" style="' + style + '"></span> ';
				innerHtml += '<tr><td>' + span + body[0] + '</td></tr>';
				innerHtml += '<tr><td>' + span + body[1] + '</td></tr>';
			});
			innerHtml += '</tbody>';

			var tableRoot = tooltipEl.querySelector('table');
			tableRoot.innerHTML = innerHtml;
		}
		var positionY = jQuery(this._chart.canvas).position().top;
		var positionX = jQuery(this._chart.canvas).position().left;

		// Display, position, and set styles for font
		tooltipEl.style.opacity = 1;
		tooltipEl.style.left = positionX + tooltip.caretX + 'px';
		tooltipEl.style.top = positionY + tooltip.caretY + 'px';
		tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
		tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
		tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
		tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
	};


	for(var i = 1; i <= total_posts; i++)
	{
		var chartData = generateChartData(i);
		if(jQuery('#myChart'+i).length > 0)
		{
			var label_data = [];
			var visits_data = [];
			var high_visit=0;
			for(var k = 0; k < chartData.length; k++)
			{
				label_data.push(chartData[k].date);
				visits_data.push(chartData[k].visits);
				if(high_visit < parseInt(chartData[k].visits))
				{
					high_visit = parseInt(chartData[k].visits);
				}
			}
				
			Chart.Tooltip.positioners.myCustomPosition = function(unused, position) {
				return { x: position.x, y: 40 }; // HARDCODING VALUES
			}	
							
			var ctx = document.getElementById('myChart'+i).getContext('2d');
			var tick_stepsize = high_visit < 5 ? 1 : 5;
			if( flag ){
				if( high_visit > 5 ){
					high_visit = high_visit + 5;
				}else{
					high_visit++;
				}
			}
			var chart = new Chart(ctx, {
			
				type: chart_type,
		
				data: {
					labels: label_data,
					datasets: [{
						backgroundColor: '#a7c1dc',
						borderColor: '#4172aa',
						data: visits_data,
						borderWidth: 1,
						
					}]
				},

				options: {
					title : {
						display : true,
						position : "down",
					},
					legend : {
						display : false,
						position : "bottom",
						
					},
					scales: {
						xAxes: [{
							ticks: {
								display: bool
							},
							gridLines:{
								display: flag,
								drawBorder: flag,
								drawOnChartArea: false,
							},				
						}],
						yAxes: [{
							ticks: {
								display: bool,
								min: 0,
								max: high_visit,
								stepSize: tick_stepsize
							},	
							gridLines:{
								display: flag,
								drawBorder: flag,
								drawOnChartArea: false,
							},
							
						}]					
					},
					tooltips: {
						enabled: false,
						mode: 'index',
						intersect: false,
						custom: customTooltips,
						callbacks: {
							label: function(tooltipItem, data) {
								var firstTooltip = "Page hits"+" : "+tooltipItem.yLabel;
								var otherTooltip = text_stat +" : "+tooltipItem.xLabel;
								var tooltip = new Array(firstTooltip, otherTooltip);	
								return tooltip;						    
							},
							title: function(tooltipItem, data) {
							  return;
							}
						},
						position: 'nearest'
					},
					elements: { 
						point: { 
							radius: 0 
						},
						line: {
							tension: 0
						} 
					},
					
					responsive: true,
					maintainAspectRatio: false,
					scaleShowLabels : false,
					
				}
			});
		}
	 }
	 
	 
if($('.counter_span').length)
{
	setInterval(sendRequest, 5000);		
}
});

function sendRequest()
{
	 var counter_arr = [];
	 $( '.counter_span' ).each(function( index ) {
		 counter_arr.push($(this).attr('data-postid'));		
	 });
		 
	 $.ajax({
		type: 'POST',
		url: minichart_object.ajaxurl,
		data: {
			'action': 'online_user_counter',
			'postids': counter_arr
		},
		success: function(data){
			data = JSON.parse(data);	
			$.each(data,function( index,value ) {
			   
				var val = $('.counter_span[data-postid="'+index+'"]').attr('data-current_val');
				$('.counter_span[data-postid="'+index+'"]').attr('data-current_val',value);
				
				var diff_counter = value - val;
				$('.wsmBoxUPDdataTIP').addClass('visible');
				window.setTimeout(function(){
					$('.wsmBoxUPDdataTIP').removeClass('visible');
				}, 2000);
				if( diff_counter < 0 )
				{
					$('.counter_span[data-postid="'+index+'"] + .live_counter').html('-'+diff_counter);
					$('.counter_span[data-postid="'+index+'"] + .wsmBoxUPDdataTIP').addClass('alert');
				}else{
					$('.counter_span[data-postid="'+index+'"] + .live_counter').html('+'+diff_counter);
					$('.counter_span[data-postid="'+index+'"] + .wsmBoxUPDdataTIP').removeClass('alert');
				}
					
				$('.counter_span[data-postid="'+index+'"]').html(value);
				
			});
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}
