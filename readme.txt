===  Visitor Statistics Minichart add-on ===
Contributors: osamaesh
Tags: histats, online counter, count visitor, google analytics, analytics, GeoIP, Geo locations, analytics dashboard, visits, statistics, stats, visitors, chart, browser, blog, today, yesterday, week, month, wp Statistics, year, post, page, sidebar, summary, feedburner, hits, pagerank, google, alexa, live visit, counter, diagram, graph, traffic
Requires at least: 3.0.1
Tested up to: 4.9.5
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Complete statistics for your WordPress site, easy to install and working fine.

== Description ==

This plugin will help you to track your visitors, browsers, operating systems, visits and much more..<br />
When it comes to ease of use, WordPress visitor statistics comes in first, You will have a real Counter and statistics plugin for your WordPress website..

Track statistics for your WordPress site without depending on external services.. On screen statistics presented as graphs are easily viewed through the WordPress admin interface.


= Features =
<ul>
<li>Comprehensive overview page & User-friendly interface</li>
<li>Fully flexible & simple to install</li>
<li>You can see the number of your visitors and online users instantly without refreshing the page.</li>
<li>Online users, visitors and visits</li>
<li>Today hits report</li>
<li>Monthly/weekly hits report</li>
<li>GeoIP location by Country</li>
<li>Interactive map of visitors location</li>
<li>Easy installation & configuration</li>
<li>Multisite support</li>
</ul>



= Support =
We're happy to help.  Here are a few things to do before contacting us:

* Have you read the [FAQs] (http://wordpress-statistics.com/Documentation/)?
* Have you search the [support forum](https://wordpress.org/support/plugin/wp-stats-manager) for a similar issue?
* You can also contact us at support@wordpress-statistics.com if you have any enquiry.



== Screenshots ==
1. Main Dashboard
2. Settings Page



== Installation ==
1. Download the package.
2. Upload the package using your plugins page > add new > upload or Extract the contents of .zip folder to wp-content/plugins/ folder
3. Activate the Plugin via plugins page
4. Clear your site cache (if you have any caching plugin enabled)

Thanks!



== Changelog ==

= 1.1 =

1. bug fixing in charts (adding the ability to display the Today, last week, last month and all the time stats)


= 1.0 =

1. Initial version
