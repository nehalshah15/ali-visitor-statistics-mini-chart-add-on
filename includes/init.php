<?php

class mini_chart_init_plugin
{	
	static private $class = null;
		
	public function __construct(){
		
		add_action('admin_enqueue_scripts', array( $this, 'chart_add_on_enqueue_script'));
		
		$settings = get_option('mini_chart_settings');
		$p_type_arr = array('post','page');
		if(!empty($settings['allow_post_type1']))
		{
			foreach($settings['allow_post_type1'] as $p_type)
			{
				add_filter( 'manage_edit-'.$p_type.'_columns', array( $this, 'hits_chart_columns') );
				if(in_array($p_type,$p_type_arr))
				{
					$p_type = $p_type.'s';
					add_action('manage_'.$p_type.'_custom_column', array( $this, 'hits_chart_columns_content'),10,2 );	
				}
				else
				{
					add_action('manage_'.$p_type.'_custom_column', array( $this, 'hits_chart_columns_content'),10,2 );	
				}
			}
		}
				
		add_filter( 'wsm_addons', array( $this, 'wsm_mini_chart_name' ) );
		add_filter( 'wsm_addons_settings', array( $this, 'wsm_mini_chart_settings' ) );
		
		add_action( 'admin_init', array( $this, 'save_mini_chart_settings' ) );
		
	}
	
	
	public function chart_add_on_enqueue_script() {  
		if ( strstr($_SERVER['SCRIPT_NAME'],'/wp-admin/edit.php') || ( isset( $_GET['page'] ) || $_GET['page'] == 'wsm_addons' ) ){  
			wp_enqueue_script('chart-js',  WSMMC_URL. 'js/Chart.min.js'); 
			wp_enqueue_script('moment-js', WSMMC_URL. 'js/moment.js');
			wp_enqueue_script('custom-js', WSMMC_URL. 'js/custom.js');
			wp_localize_script('custom-js', 'minichart_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			
			wp_enqueue_style('custom-css', WSMMC_URL. 'css/custom.css');
			
			wp_enqueue_style( 'select2.min-1', WSMMC_URL . 'css/select2.min.css');
			wp_enqueue_script( 'select2-1', WSMMC_URL . 'js/select2.min.js' );
		}			
	}
				
	public function hits_chart_columns( $columns ) {
		
		$settings = get_option('mini_chart_settings');
				
		if($settings['mini_chart_enable'])
		{
			if($settings['online_visitors']){
				$columns['online'] = __('Online');
			}
			
			if($settings['hits']){
				$columns['hits'] = __('Hits');
			}

			if($settings['charts_enable']){
				$columns['hits_chart'] = __('Hits chart'); 
			}
		}

		return $columns;
	}
	
	
	
	public function hits_chart_columns_content($column,$post_id)
	{
		
		global $wpdb;
		$table_prefix = $wpdb->prefix.P_PREFIX;
		$settings = get_option('mini_chart_settings');
		
		$date_prefix = 'DATE_FORMAT(CONVERT_TZ(LV.serverTime,"+00:00","'.WSM_TIMEZONE.'"),"%k") as date';
		$start_date = $end_date = date('Y-m-d');
		$type = isset( $settings[ 'stats_options' ] ) ? $settings[ 'stats_options' ] : '';
		switch( $type ){
			case 'week':
				$date_prefix = 'DATE_FORMAT(CONVERT_TZ(LV.serverTime,"+00:00","'.WSM_TIMEZONE.'"),"%Y-%m-%d") as date';
				$start_date = date('Y-m-d', strtotime('-6 days'));	
				break;
			case 'month':
				$date_prefix = 'DATE_FORMAT(CONVERT_TZ(LV.serverTime,"+00:00","'.WSM_TIMEZONE.'"),"%Y-%m-%d") as date';
				$start_date = date('Y-m-d', strtotime('-30 days'));	
				break;
			case 'year':
				$date_prefix = 'DATE_FORMAT(CONVERT_TZ(LV.serverTime,"+00:00","'.WSM_TIMEZONE.'"),"%c") as date';
				$start_date = date('Y-01-01');	
				break;
		}
		
		switch ($column)
		{
			case "hits_chart":
				global $wp_query;
				$wp_query->the_post();
				$total_posts = $wp_query->post_count;
				$i = $wp_query->current_post + 1 ;			
				
				$visitLastActionTime="CONVERT_TZ(LV.serverTime,'+00:00','".WSM_TIMEZONE."')";
				$conditional_query = " {$visitLastActionTime} >= '".$start_date.' 00:00:00'."' AND {$visitLastActionTime}<='".$end_date.' 23:59:59'."'";
						
				$protocol_arr = array('http://','https://','www.');		
				$link = str_replace($protocol_arr,'',get_permalink($post_id));	
				if(substr($link,-1) == '/')
				{
					$link = substr($link,0,-1);
				}
					
				$query1 = "select count(DISTINCT LV.id) AS total_page_views,$date_prefix
				FROM ".$table_prefix."_logVisit AS LV 
				LEFT JOIN ".$table_prefix."_url_log AS UL on UL.id = LV.URLId 
				WHERE  $conditional_query AND (UL.pageId = $post_id OR UL.url like '%$link%') GROUP BY date ORDER BY total_page_views DESC";
				
				$result = $wpdb->get_results($query1);		
								
				$d  = array();
				$dt = array();
				$months = array(
				    'January',
				    'February',
				    'March',
				    'April',
				    'May',
				    'June',
				    'July ',
				    'August',
				    'September',
				    'October',
				    'November',
				    'December',
				);
				
				if(!empty($result))
				{		
					//$start_date = date('Y-m-d', strtotime('-6 days'));
					$last_date = 0;
					$exist_date = array();
					
					foreach($result as $res)
					{
						if($settings['stats_options'] == 'today')
						{
							$exist_date[$res->date] = $res->total_page_views;
						}	
						if($settings['stats_options'] == 'week')
						{
							$exist_date[$res->date] = $res->total_page_views;
						}
						if($settings['stats_options'] == 'month')
						{
							$exist_date[$res->date] = $res->total_page_views;
						}
						if($settings['stats_options'] == 'year')
						{
							$exist_date[$res->date] = $res->total_page_views;
						}
							
					}
					
					if($settings['stats_options'] == 'today')
					{
						for ( $h = 0; $h < 24; $h++ ) {
							$d['visits'] =  isset( $exist_date[ $h ] ) ? $exist_date[ $h ] : 0;
							$d['date'] = $h + 1;
							$dt[] = $d;
						}
						echo '<input type="hidden" name="stat_type" id="stat_type" value="today">';
					}
					
					if($settings['stats_options'] == 'week')
					{
						for ( $h = 0; $h < 7; $h++ ) {
							$l_date = date( 'Y-m-d', strtotime( '+'. $h .' Day', strtotime($start_date) ) ); 
							$d['visits'] =  isset( $exist_date[ $l_date ] ) ? $exist_date[ $l_date ] : 0;
							$d['date'] = $l_date;
							$dt[] = $d;
						}
						echo '<input type="hidden" name="stat_type" id="stat_type" value="week">';
					}
					
					if($settings['stats_options'] == 'month')
					{	
						$month = date("m",strtotime($start_date));
						$year = date("Y",strtotime($start_date));
						
						//$total_days = cal_days_in_month(CAL_GREGORIAN, $month ,$year);
						
						for ( $h = 0; $h <= 30; $h++ ) {
							$c_date = date( 'Y-m-d', strtotime( '+'. $h .' Day', strtotime($start_date) ) ); 
							$d['visits'] =  isset( $exist_date[ $c_date ] ) ? $exist_date[ $c_date ] : 0;
							$d['date'] = $c_date;
							$dt[] = $d;
						}
						echo '<input type="hidden" name="stat_type" id="stat_type" value="month">';
					}
					
					if($settings['stats_options'] == 'year')
					{
						
						for ( $h = 1; $h <= date('n'); $h++ ) {
							$d['visits'] =  isset( $exist_date[ $h ] ) ? $exist_date[ $h ] : 0;
							$d['date'] = __( $months[$h - 1] , 'wsm' );
							$dt[] = $d;
						}
						echo '<input type="hidden" name="stat_type" id="stat_type" value="year">';
					}
															
					$data = json_encode($dt);
																
					echo '<div id="chart-cont" class="chart-container">';
					echo '<canvas id="myChart'.$i.'" data-stuff='.$data.' class="chart chart-line"></canvas>';
					echo '</div>';
									
					echo '<input type="hidden" name="total_posts" id="total_posts" value="'.$total_posts.'">';
					
					if($settings['charts'] == 'line')
					{
						echo '<input type="hidden" name="chart_type" id="chart_type" value="line">';
					}
					else
					{
						echo '<input type="hidden" name="chart_type" id="chart_type" value="bar">';	
					}
				}
				else
				{
					echo '<img src="'. WSMMC_URL.'images/no-data-label.svg'.'" height="70"/>';
				}
							
			break;
			
			case "online":
							
				$newTimeZone=wsmCurrentGetTimezoneOffset();
								
				$visitLastActionTime="CONVERT_TZ(LV.serverTime,'+00:00','".$newTimeZone."')";
				$conditional_query = " {$visitLastActionTime} >= '".wsmGetDateByInterval('-'.WSM_ONLINE_SESSION.' minutes')."'";
				
				$protocol_arr = array('http://','https://','www.');		
				$link = str_replace($protocol_arr,'',get_permalink($post_id));	
				if(substr($link,-1) == '/')
				{
					$link = substr($link,0,-1);
				}				
				$query = 'SELECT LV.id FROM '.$table_prefix.'_logVisit AS LV LEFT JOIN '.$table_prefix.'_url_log AS UL ON LV.URLId = UL.id WHERE '.$conditional_query.' AND (UL.pageId = '.$post_id.' OR UL.url like "%'.$link.'%") group by LV.visitorId';
								
				$count = $wpdb->query($query);
				
				echo '<center class="counter_center"><span class="counter_span" data-postid="'.$post_id.'" data-current_val="'.$count.'">'.number_format($count).'</span><span class="wsmBoxUPDdataTIP live_counter" data-live_c="'.$count.'"></span></span></center>';
												
			break;
			
			case "hits":
				
				$visitLastActionTime="CONVERT_TZ(LV.serverTime,'+00:00','".WSM_TIMEZONE."')";
				$conditional_query = " {$visitLastActionTime} >= '".$start_date.' 00:00:00'."' AND {$visitLastActionTime}<='".$end_date.' 23:59:59'."'";
						
				$protocol_arr = array('http://','https://','www.');		
				$link = str_replace($protocol_arr,'',get_permalink($post_id));	
				if(substr($link,-1) == '/')
				{
					$link = substr($link,0,-1);
				}
					
				$query = "select count(DISTINCT LV.id) AS total_page_views
				FROM ".$table_prefix."_logVisit AS LV 
				LEFT JOIN ".$table_prefix."_url_log AS UL on UL.id = LV.URLId 
				WHERE  $conditional_query AND (UL.pageId = $post_id OR UL.url like '%$link%')";
				
				$result = $wpdb->get_var($query);	
				echo '<center><span>'.number_format($result).'</span></center>';
										
			break;
		}
	}
	
	
	public function wsm_mini_chart_name( $name ){
		$name[ 'mini_chart' ] = __( 'Mini chart Add-on', 'wsm' );
		return $name;
	}
	
	public function wsm_mini_chart_settings( $settings ){
		$settings[ 'mini_chart' ] = array(
		
			array(
				'id' => 'charts_enable',
				'type'	=>	'checkbox',
				'label'	=>	__( 'Chart enable', 'wsm' )
			),
			
			array(
				'id' => 'charts',
				'type'	=>	'select',
				'label'	=>	__( 'Chart', 'wsm' ),
				'values'	=>	array(
					'line'	=>	__( 'Line chart', 'wsm' ),
					'column'=>	__( 'Column chart', 'wsm' )		
				),
				'default' => 'line'
			),
			array(
				'id' => 'hits',
				'type'	=>	'checkbox',
				'label'	=>	__( 'Hits', 'wsm' )
			),
			array(
				'id' => 'online_visitors',
				'type'	=>	'checkbox',
				'label'	=>	__( 'Online visitors', 'wsm' )
			),
			array(
				'id' => 'stats_options',
				'type'	=>	'select',
				'label'	=>	__( 'Display chart for', 'wsm' ),
				'values'	=>	array(
					'today'	=>	__( 'Today stats', 'wsm' ),
					'week'	=>	__( 'Last 7 days', 'wsm' ),
					'month'	=>	__( 'Last 30 days', 'wsm' ),
					'year'	=>	__( 'All the time', 'wsm' ),
				),
				'default' => 'month'
			),
			array(
				'id' => 'allow_post_type1',
				'type'	=>	'post_type',
				'label'	=>	__( 'Available for', 'wsm' ),
				'default' => array( 'post', 'page' )
			),
		);
		return $settings;
	}
	
	
	public function save_mini_chart_settings(){
	
		if( isset( $_POST['action'] ) && $_POST['action'] == 'save_wsm_addons' ){
			$setting_value['mini_chart_enable'] = isset( $_POST[ 'mini_chart_enable' ] ) ? 1 : 0;
			$settings = $this->wsm_mini_chart_settings($settings);
			foreach( $settings[ 'mini_chart' ] as $setting ){
				$setting_value[ $setting['id'] ]	=  isset( $_POST[ $setting['id'] ] ) ? $_POST[ $setting['id'] ] : '';
			}
			update_option( 'mini_chart_settings', $setting_value ); 
		}
		
		
		add_action( 'wp_ajax_online_user_counter', array( $this, 'online_user_counter') );
	}
	
	static function initMC(){
		if( null === self::$class ){
			self::$class = new self();
		}
		return self::$class;
	}
	
	public function online_user_counter()
	{
		global $wpdb;
		$post_ids = $_POST['postids']; 
		$counter_arr = array();
		foreach($post_ids as $post_id)
		{				
			$newTimeZone=wsmCurrentGetTimezoneOffset();
			$table_prefix = $wpdb->prefix.P_PREFIX;						
			$visitLastActionTime="CONVERT_TZ(LV.serverTime,'+00:00','".$newTimeZone."')";
			$conditional_query = " {$visitLastActionTime} >= '".wsmGetDateByInterval('-'.WSM_ONLINE_SESSION.' minutes')."'";
			
			$protocol_arr = array('http://','https://','www.');		
			$link = str_replace($protocol_arr,'',get_permalink($post_id));	
			if(substr($link,-1) == '/')
			{
				$link = substr($link,0,-1);
			}				
			$query = 'SELECT LV.id FROM '.$table_prefix.'_logVisit AS LV LEFT JOIN '.$table_prefix.'_url_log AS UL ON LV.URLId = UL.id WHERE '.$conditional_query.' AND (UL.pageId = '.$post_id.' OR UL.url like "%'.$link.'%") group by LV.visitorId';
							
			$count = $wpdb->query($query);
			
			$counter_arr[$post_id] = $count;
			
		}
		echo json_encode($counter_arr);
		die;
	}
		
}
?>
