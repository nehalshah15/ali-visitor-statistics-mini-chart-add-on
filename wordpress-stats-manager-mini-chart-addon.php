<?php
/**
 * Plugin Name: visitor statistics mini-chart add-on
 * Plugin URI: http://plugins-market.com/products/
 * Description: This plugin will display chart statistics based on user views of post.
 * Version: 1.1
 * Author: osamaesh 
 * Author URI: http://plugins-market.com/contact-us/
 * Developer: Prism I.T. Systems
 * Developer URI: http://www.prismitsystems.com
 * Text Domain: wp-stats-manager-mini-chart
 * Domain Path: /languages
 * Copyright: © 2009-2017 PRISM IT SYSTEMS.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
**/
if ( ! defined( 'ABSPATH' ) ) {
    die( 'Access denied.' );
}


define( 'WSMMC_DIR', plugin_dir_path( __FILE__ ) );
define( 'WSMMC_URL', plugin_dir_url( __FILE__ ) );

if( wsmmc_requirements_check() ){
	include_once(WSMMC_DIR .'includes/init.php');
	mini_chart_init_plugin::initMC();
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://plugins-market.com/buyplugin/wp-update-server-php7/?action=get_metadata&slug=wordpress-stats-manager-mini-chart-addon',
    __FILE__,
	'wordpress-stats-manager-mini-chart-addon'
);



function wsmmc_requirements_check(){
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'wp-stats-manager/wp-stats-manager.php' ) || is_plugin_active( 'wordpress-stats-manager-pro/wordpress-stats-manager-pro.php' ) ) {
		return true;
	}		
	return false;
}
